
import UIKit

class RootVC {
  
  public func set(window: UIWindow?){
    let rootViewController = UIStoryboard.createVC(sbID: .Loading)
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
  public func setScene(windowScene: UIWindowScene?, window: UIWindow?){
    let rootViewController = UIStoryboard.createVC(sbID: .Loading)
    window?.windowScene = windowScene
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
}

