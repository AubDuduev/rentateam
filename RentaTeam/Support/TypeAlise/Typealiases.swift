
import Foundation

//MARK:- RESULTS CLOSURE
typealias СlosurePictureSession = ((PictureSessionResult) -> Void)
typealias СlosureServerResult   = ((ServerResult) -> Void)
typealias СlosureDecode         = ((DecodeResult) -> Void)
typealias СlosureSession        = ((SessionResult) -> Void)
typealias СlosureSessionData    = ((SessionDataResult) -> Void)
typealias СlosureRequest        = ((RequestResult) -> Void)
typealias СlosureResultFB       = ((FirebaseResult) -> Void)

//MARK: - CLOUSURES
typealias СlosureReturn<T>      = (() -> T)
typealias Сlosure<T>            = ((T) -> Void)
typealias СlosureEmpty          = (() -> Void)
typealias СlosureTwo<T, G>      = ((T, G) -> Void)
typealias СlosureAny            = ((Any?) -> Void)

//MARK: - CUSTOM TYPE
typealias Header    = [String: String]
typealias ReturnURL = (string: String?, URL: URL?)


