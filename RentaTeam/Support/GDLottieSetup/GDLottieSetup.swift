import UIKit
import Lottie

class GDLottieSetup {
  
  private let view          : UIView!
  private var animationView : AnimationView!
  private var animation     : Animation!
  
  public var callbackPlay: Сlosure<Bool>!
  
  public func added(name: String, loopMode: LottieLoopMode, speed: CGFloat = 1){
    
    self.animation     = Animation.named(name)
    self.animationView = AnimationView(animation: animation)
    
    animationView.contentMode = .scaleAspectFit
    animationView.animation   = animation
    animationView.loopMode    = loopMode
    
    animationView.animationSpeed = speed
    view.insertSubview(animationView, at: 0)
  }
  public func setup(size: CGFloat = 0, padding: CGFloat = 0){
    let width : CGFloat = view.frame.width
    let height: CGFloat = view.frame.height
    animationView?.frame = CGRect(x    : 0 - padding,
                                 y     : 0 - padding,
                                 width : width + size,
                                 height: height + size)
  }
  public func play(){
    animationView.play { (finish) in
      self.callbackPlay?(finish)
    }
  }
  public func remove(){
    self.animationView?.removeFromSuperview()
  }
  public func playProgress(fromProgress: AnimationProgressTime, toProgress: AnimationProgressTime){
    animationView.play(fromProgress: fromProgress, toProgress: toProgress, loopMode: .playOnce)
  }
  public func stop(){
    self.animationView.currentProgress = 0
  }
  public func progress(value: CGFloat){
    self.animationView.currentProgress = value
  }
  enum LottieName: String {
    
    case equalizer
    case loading
    case equalizerBig
    case non
  }
  init(view: UIView) {
    self.view = view
  }
}
