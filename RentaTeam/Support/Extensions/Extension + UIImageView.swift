
import UIKit

extension UIImageView {
	
	func set(nameImage: NameImage){
		self.image = UIImage(named: nameImage.rawValue)
	}
  func set(system: NameImage){
    self.image = UIImage(systemName: system.rawValue)
  }
	
	enum NameImage: String {
		
    case play  = "play.fill"
    case pause = "pause.fill"
	}
}



