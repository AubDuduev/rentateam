//
import AVKit

extension AVPlayer {
  
  public func duration() -> (second: Int, minutes: Int, seconds: Float)? {
    if let duration = self.currentItem?.duration {
      let seconds = CMTimeGetSeconds(duration)
      guard !seconds.isNaN else { return nil }
      let second  = Int(seconds) % 60
      let minutes = Int(seconds) / 60
      return (second, minutes, Float(seconds))
    }
    return nil
  }
}
