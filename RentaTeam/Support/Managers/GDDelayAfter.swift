import Foundation

class DelayAfter {
  
  static let shared = DelayAfter()
  private let queue = OperationQueue()
  public var execute: СlosureEmpty!
  
  public func deleteAfter(seconds: Double) {
    let date = Date(timeIntervalSinceNow: seconds)
    self.queue.schedule(after: .init(date)) { [weak self] in
      guard let self = self else { return }
      self.execute()
    }
  }
  public func cancelDelete() {
    queue.cancelAllOperations()
  }
  
  private init(){}
}


