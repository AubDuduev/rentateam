
import UIKit

class ErrorHandlerSearchResult {
   
  public func check(searchResult: DECSearchResult?) -> Bool {
    do {
      try self.error(searchResult: searchResult)
    } catch HandlerError.Success {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, message: .message(HandlerError.Nil.rawValue))
      return false
    } catch HandlerError.Empty {
      AlertEK.customText(title: .error, message: .message(HandlerError.Empty.rawValue))
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
  private func error(searchResult: DECSearchResult?) throws  {
    
    //Ошибка получения
    guard let searchResult = searchResult else {
      throw HandlerError.Nil
    }
    
    //Сообщения получены , но их нет
    guard let artists = searchResult.results else {
      throw HandlerError.Empty
    }
    //Сообщения получены , но их нет
    if artists.isEmpty {
      throw HandlerError.Empty
    }
    
    //Ошибки не обнаружены
    throw HandlerError.Success
  }
  
  private enum HandlerError: String, Error {
    
    case Nil     = "Ошибка получения данных"
    case Empty   = "По вашему запросу нечего не найдено"
    case Success
  }
}


