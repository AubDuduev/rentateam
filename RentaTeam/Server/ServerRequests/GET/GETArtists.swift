
import Foundation
//GET запрос на получение списка артистов по выбранному имени
//https://itunes.apple.com/search?term={artistsName}

class GETArtists: Requestoble, Sessionoble {
   
   private var urls       = URLAbstractBase()
   private var parameters = URLParameters()
   private var headers    = URLHeaders()
   private var jsonDecode = JSONDecode()
   private var urlBody    = URLBody()
   private var group      = DispatchGroup()
   
   public func request(data: Any?, completionRequest: @escaping СlosureRequest) {
      
      let artistName = data as! String
      let url = self.urls.create(type: .SearchArtists(name: artistName))?.URL
      var sessionData: Data!
      self.group.enter()
      //Получение данных
      session(url: url, httpMethod: .get) { sessionResult in
         switch sessionResult {
            //Success
            case .data(let data):
               sessionData = data
               self.group.leave()
            //Error
            case .error(let error):
               print(error!.localizedDescription, "Error JSONDecode get from Server ")
               completionRequest(.error(error))
               self.group.leave()
         }
      }
      self.group.notify(queue: .main) {
         //Декодинг данных
         self.jsonDecode.decode(jsonType: DECSearchResult.self, data: sessionData) { (decodeResult) in
            //Decoding
            switch decodeResult {
               //Error
               case .error(let error):
                  completionRequest(.error(error))
               //Suссes
               case .json(let object):
                  if let searchResult = object as? DECSearchResult {
                     completionRequest(.object(searchResult))
                  }
            }
         }
      }
   }
}
