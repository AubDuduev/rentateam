
import Foundation

struct DECArtist {
  
  let artistID         : Int?
  let collectionID     : Int?
  let trackID          : Int?
  let artistName       : String?
  let collectionName   : String?
  let trackName        : String?
  let trackViewURL     : String?
  let collectionURL    : String?
  let trackURL          : String?
  
  enum CodingKeys: String, CodingKey {
    
    case artistID        = "artistId"
    case collectionID    = "collectionId"
    case trackID         = "trackId"
    case artistName
    case collectionName
    case trackName
    case trackViewURL    = "trackViewUrl"
    case collectionURL   = "artworkUrl100"
    case trackURL        = "previewUrl"
  }
}
extension DECArtist: Decodable {
  
  init(from decoder: Decoder) throws {
    
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.artistID       = try? values.decode(Int?.self   , forKey: .artistID      )
    self.collectionID   = try? values.decode(Int?.self   , forKey: .collectionID  )
    self.trackID        = try? values.decode(Int?.self   , forKey: .trackID       )
    self.artistName     = try? values.decode(String?.self, forKey: .artistName    )
    self.collectionName = try? values.decode(String?.self, forKey: .collectionName)
    self.trackName      = try? values.decode(String?.self, forKey: .trackName     )
    self.trackViewURL   = try? values.decode(String?.self, forKey: .trackViewURL  )
    self.collectionURL  = try? values.decode(String?.self, forKey: .collectionURL )
    self.trackURL       = try? values.decode(String?.self, forKey: .trackURL )
  }
}


