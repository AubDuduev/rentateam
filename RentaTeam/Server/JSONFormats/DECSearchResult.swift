
import Foundation

struct DECSearchResult {
  
  let resultCount: Int?
  let results    : [DECArtist]?
  
  enum CodingKeys: String, CodingKey {
    
    case resultCount
    case results
  }
}
extension DECSearchResult: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.resultCount = try? values.decode(Int?.self        , forKey: .resultCount)
    self.results     = try? values.decode([DECArtist]?.self, forKey: .results)
  }
}
