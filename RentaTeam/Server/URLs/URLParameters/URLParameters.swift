
import Foundation

class URLParameters: NSObject {
  
  private var queryKeyQueryValue  : [QueryKey: QueryValue]!
  private var queryKeyStringValue : [QueryKey: String]!
  private var stringKeyStringValue: [String: String]!
  
  private let messageSMS = "Ваш код для входа в приложение"
  
  enum QueryItemsType {
    case searchArtist(String)
  }
  
  enum QueryKey: String {
    
    case none
    case term
  }
  enum QueryValue: String {
    
    case none
    case searchArtist
  }
  enum QueryValueString {
    
    case none
  }
  enum QueryKeyStringValueString {
    
    case none
  }
  enum DictionaryType {
    
    case QueryKeyQueryValue([QueryKey: QueryValue])
    case QueryKeyStringValue([QueryKey: String])
    case StringKeyStringValue([String: String])
  }
  
  //MARK: - Функция для создании параметров для URL
  public func create(queryItems: QueryItemsType, _ valueString: QueryValueString = .none, _ queryKeyStringValueString: QueryKeyStringValueString = .none) -> [URLQueryItem]? {
    switch queryItems {
      //Параметр (имя артиста) для URL
      //https://itunes.apple.com/search?term={artistsName}
      case .searchArtist(let artistsName):
        self.queryKeyStringValue = [.term: artistsName]
        return self.createQueryItems(dictionaryType: .QueryKeyStringValue(self.queryKeyStringValue))
    }
  }
  
  //MARK: - Функция для генерации параметров для URL
  //пробегается с помощью MAP по словарю и выдает тип [URLQueryItem]
  private func createQueryItems(dictionaryType: DictionaryType) -> [URLQueryItem] {
    switch dictionaryType {
      
      //Создаем параметр из статических Key/Value
      case .QueryKeyQueryValue(let dictionary):
        return dictionary.map{ URLQueryItem(name: $0.rawValue, value: $1.rawValue) }
        
      //Создаем параметр из статический Key и динамического Value
      case .QueryKeyStringValue(let dictionary):
        return dictionary.map{ URLQueryItem(name: $0.rawValue, value: $1) }
        
      //Создаем параметр из динамического Key и динамического Value
      case .StringKeyStringValue(let dictionary):
        return dictionary.map{ URLQueryItem(name: $0, value: $1) }
    }
  }
}


