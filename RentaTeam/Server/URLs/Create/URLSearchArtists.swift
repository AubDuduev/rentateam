
import Foundation


class URLSearchArtists: URLCreatoble {
  
  private var createPath = URLPath()
  private var custom     = URLCreateCustom()
  private var parameter  = URLParameters()
  private let createHost = URLHost()
  
  public func url(_ type: URLAbstractFactory.Types) -> ReturnURL? {
    
    switch type {
      case .SearchArtists(let artistsName):
        //1 - Параметр для URL
        let queryItems = parameter.create(queryItems: .searchArtist(artistsName))
        let path       = createPath.create(type: .search)
        let host       = createHost.create(.Static(.iTunes))
        let createURL  = URLCreateCustom.Custom(scheme: .https, host: host, path: path, queryItems: queryItems)
        let customURL  = self.custom.create(type: createURL)
        //2 - Возвращаем его в классе -> URLAbstractBase -> GETArtists
        return (customURL.string, customURL.URL)
      default:
         return nil
    }
  }
}
//Создание URL при поиске артиста
//artistsName - это поисковое слово
//https://itunes.apple.com/search?term={artistsName}
