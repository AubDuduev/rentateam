
import Foundation

class URLAbstractBase {
  
  //Private
  private let type  = URLAbstractFactory()
  
  //Public
  public let string = URLStaticString()
  
  public func create(type: URLAbstractFactory.Types) -> ReturnURL? {
    return self.type.set(type).url(type)
  }
}

