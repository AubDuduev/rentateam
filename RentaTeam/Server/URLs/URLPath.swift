
import Foundation

class URLPath {
  
  
  enum Path: String {

    case non = ""
    case search = "/search"
  }
  
  public func create(type: Path) -> String {
    //create Change Path for url
    switch type {
      //MARK: - Non
      case .non:
        return ""
      case .search:
        return type.rawValue
    }
  }
}
