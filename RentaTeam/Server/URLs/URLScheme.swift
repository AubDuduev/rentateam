
import Foundation

struct URLScheme {
  
  enum Scheme: String {
    
    case http
    case https
    case wss
    case ws
  }
}
