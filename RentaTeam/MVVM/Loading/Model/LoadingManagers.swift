
import Foundation

class LoadingManagers: VMManagers {
  
  let setup : SetupLoading
  let router: RouterLoading
  
  init(viewModel: LoadingViewModel) {
    
    self.setup  = SetupLoading(viewModel: viewModel)
    self.router = RouterLoading(viewModel: viewModel)
  }
}

