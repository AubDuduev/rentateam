import UIKit

class LoadingViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = LoadingViewModel(viewController: self)
   
   //MARK: - Public variable
   
   
   //MARK: - Outlets
   @IBOutlet weak var lottieView: UIView!
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      self.viewModel.viewDidAppear()
   }
}
