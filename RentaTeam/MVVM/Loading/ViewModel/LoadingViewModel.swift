
import Foundation

class LoadingViewModel: VMManagers {
   
   public var loadingModel: LoadingModel = .loading {
      didSet{
         self.loadingLogic()
      }
   }
   
   //MARK: - Public variable
   unowned public var managers: LoadingManagers {
      return LoadingManagers(viewModel: self)
   }
   
   public var VC         : LoadingViewController!
   public var setupLottie: GDLottieSetup!
   
   public func viewDidLoad() {
      self.managers.setup.setupLottie()
   }
   
   public func viewDidAppear(){
      DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
         guard let self = self else { return }
         self.managers.router.push(.MainVC)
      }
   }
   
   public func loadingLogic(){
      
      switch self.loadingModel {
         //1 - Загрузка
         case .loading:
            print("")
         //2 - Получаем данные
         case .getData:
            print("")
         //3 - Проверяем на ошибки
         case .errorHandler:
            print("")
         //4 - Презентуем данные
         case .presentData(_):
            print("")
      }
   }
}
//MARK: - Initial
extension LoadingViewModel {
   
   convenience init(viewController: LoadingViewController) {
      self.init()
      self.VC = viewController
   }
}
