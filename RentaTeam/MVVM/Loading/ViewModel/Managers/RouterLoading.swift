
import UIKit

class RouterLoading: VMManager {
   
   //MARK: - Public variable
   unowned public var VM: LoadingViewModel!
   
   public func push(_ type: Push){
      
      switch type {
         case .MainVC:
            self.pushMainVC()
      }
   }
   
   enum Push {
      case MainVC
   }
}
//MARK: - Private functions
extension RouterLoading {
   
   private func pushMainVC(){
      let mainVC = self.VM.VC.getVCForID(storyboardID     : .Main,
                                         vcID             : .MainVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! MainViewController
      self.VM.VC.present(mainVC, animated: true)
   }
}
//MARK: - Initial
extension RouterLoading {
   
   //MARK: - Inition
   convenience init(viewModel: LoadingViewModel) {
      self.init()
      self.VM = viewModel
   }
}



