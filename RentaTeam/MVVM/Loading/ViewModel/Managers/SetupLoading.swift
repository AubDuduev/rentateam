
import UIKit

class SetupLoading: VMManager {
  
  //MARK: - Public variable
  unowned public var VM: LoadingViewModel!
  
  public func setupLottie(){
    self.VM.setupLottie = GDLottieSetup(view: self.VM.VC.lottieView)
    let name = GDLottieSetup.LottieName.loading.rawValue
    self.VM.setupLottie.added(name: name, loopMode: .autoReverse)
    self.VM.setupLottie.setup(size: 0, padding: 0)
    self.VM.setupLottie.play()
  }
}
//MARK: - Initial
extension SetupLoading {
  
  //MARK: - Inition
  convenience init(viewModel: LoadingViewModel) {
    self.init()
    self.VM = viewModel
  }
}


