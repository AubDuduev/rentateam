
import UIKit

enum DetailModel {
	
	case loading
	case errorHandler(DetailData?)
	case presentData(DetailData)
	
	
}

