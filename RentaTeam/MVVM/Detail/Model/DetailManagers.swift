
import Foundation

class DetailManagers: VMManagers {
  
  let setup  : SetupDetail!
  let present: PresentDetail!
  let logic  : LogicDetail!
 
  init(viewModel: DetailViewModel) {
    
    self.setup   = SetupDetail(viewModel: viewModel)
    self.present = PresentDetail(viewModel: viewModel)
    self.logic   = LogicDetail(viewModel: viewModel)
  }
}

