import UIKit

class DetailViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = DetailViewModel(viewController: self)
   
   //MARK: - Public variable
   public var detailData : DetailData!
   public var setupLottie: GDLottieSetup!
   public var player     = GDPlayer()
   
   //MARK: - Outlets
   @IBOutlet weak var backgroundImageView: UIImageView!
   @IBOutlet weak var artistNameLabel    : UILabel!
   @IBOutlet weak var trackNameLabel     : UILabel!
   @IBOutlet weak var collectionNameLabel: UILabel!
   @IBOutlet weak var lottieView         : UIView!
   @IBOutlet weak var playButton         : UIButton!
   @IBOutlet weak var timeSlider         : UISlider!
   @IBOutlet weak var timeEndLabel       : UILabel!
   @IBOutlet weak var timeBeginLabel     : UILabel!
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      self.viewModel.viewWillDisappear()
   }
}
