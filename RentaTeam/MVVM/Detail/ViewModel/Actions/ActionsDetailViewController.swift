//
//  ActionsDetailViewController.swift
//  RentaTeam
//
//  Created by Senior Developer on 28.02.2021.
//
import UIKit

extension DetailViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func playButton(button: UIButton){
    self.viewModel.managers.logic.playPause()
  }
  @IBAction func sliderButton(slider: UISlider){
    self.viewModel.managers.present.changeValueSlider(slider: slider)
  }
}
