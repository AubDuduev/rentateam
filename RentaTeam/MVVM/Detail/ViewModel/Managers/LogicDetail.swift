import Foundation
import UIKit

class LogicDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModel!
  
  public func playPause(){
    if self.VM.VC.player.control.isPlay {
      self.VM.VC.player.control.pause()
      self.VM.VC.setupLottie.stop()
      self.VM.VC.playButton.setBackgroundImage(UIImage.set(system: .pause), for: .normal)
    } else {
      self.VM.VC.player.control.play()
      self.VM.VC.setupLottie.play()
      self.VM.VC.playButton.setBackgroundImage(UIImage.set(system: .play), for: .normal)
    }
  }
  public func pause(){
    self.VM.VC.player.control.pause()
    self.VM.VC.setupLottie.stop()
    self.VM.VC.playButton.setBackgroundImage(UIImage.set(system: .pause), for: .normal)
  }
}
//MARK: - Initial
extension LogicDetail {
  
  //MARK: - Inition
  convenience init(viewModel: DetailViewModel) {
    self.init()
    self.VM = viewModel
  }
}
