
import UIKit
import SkeletonView
import AVKit

class PresentDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModel!
  
  public func image(artist: DECArtist){
    guard let imageURL = artist.collectionURL else { return }
    guard let url = URL(string: imageURL) else { return }
    self.VM.VC.backgroundImageView.isSkeletonable = true
    self.VM.VC.backgroundImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    self.VM.VC.backgroundImageView.sd_setImage(with: url) { (_, error, _, url) in
      
      if let error = error {
        print("Error load image , Class: ", "Localised: \(error.localizedDescription)")
      } else {
        self.VM.VC.backgroundImageView.hideSkeleton()
      }
    }
  }
  public func data(artist: DECArtist){
    self.VM.VC.artistNameLabel.text     = artist.artistName
    self.VM.VC.trackNameLabel.text      = artist.trackName
    self.VM.VC.collectionNameLabel.text = artist.collectionName
  }
  public func videoTimeEnd(){
    guard self.VM.VC.player.control.isPlay else { return }
    let second  = self.VM.VC.player.player.duration()?.second ?? 0
    let minutes = self.VM.VC.player.player.duration()?.minutes ?? 0
    self.VM.VC.timeEndLabel.text = String(format: .format(.two), minutes, second)
  }
  public func videoTimeStart(currentTime: CMTime){
    guard self.VM.VC.player.control.isPlay else { return }
    let seconds = CMTimeGetSeconds(currentTime)
    let second  = Int(seconds) % 60
    let minutes = Int(seconds) / 60
    self.VM.VC.timeBeginLabel.text = String(format: .format(.two), minutes, second)
  }
  public func videoTimeSlider(currentTime: CMTime){
    let currentSeconds = Float(CMTimeGetSeconds(currentTime))
    let totalSeconds   = self.VM.VC.player.player.duration()?.seconds ?? 0
    let sliderValue    = Float(currentSeconds / totalSeconds)
    self.VM.VC.timeSlider.value = sliderValue
    guard sliderValue == 1.0 else { return }
    self.VM.VC.timeSlider.value = 0
    self.VM.VC.player.control.pause()
  }
  public func changeValueSlider(slider: UISlider){
    let slider  = slider.value
    let seconds = self.VM.VC.player.player.duration()?.seconds ?? 0
    let value   = CMTimeValue(slider * seconds)
    let seeTime = CMTime(value: value, timescale: 1)
    self.VM.VC.player.player.seek(to: seeTime)
  }
}
//MARK: - Initial
extension PresentDetail {
  
  //MARK: - Inition
  convenience init(viewModel: DetailViewModel) {
    self.init()
    self.VM = viewModel
  }
}

