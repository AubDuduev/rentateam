
import UIKit

class SetupDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModel!
  
  public func setupLottie(){
    self.VM.VC.setupLottie = GDLottieSetup(view: self.VM.VC.lottieView)
    let name = GDLottieSetup.LottieName.equalizerBig.rawValue
    self.VM.VC.setupLottie.setup(size: 0, padding: 0)
    self.VM.VC.setupLottie.added(name: name, loopMode: .autoReverse)
  }
  public func player(){
    self.VM.VC.player.load(urlPlayer: .network(self.VM.VC.detailData.artist?.trackURL), typeURL: .mp4)
  }
  public func playButton(){
    self.VM.VC.playButton.setBackgroundImage(UIImage.set(system: .pause), for: .normal)
  }
  public func addObserverReturnTimeVideoPlayer(){
    self.VM.VC.player.control.observerReturnTime = { (time) in
      //3 - set value slider
      self.VM.managers.present.videoTimeSlider(currentTime: time)
      //4 - set time begin label
      self.VM.managers.present.videoTimeStart(currentTime: time)
      //5 - set time end label
      self.VM.managers.present.videoTimeEnd()
    }
  }
  public func playerEnd(){
    self.VM.VC.player.control.observerEnd = {
      self.VM.VC.player.control.pause()
      self.VM.VC.setupLottie.stop()
    }
  }
  public func timeSlider(){
    self.VM.VC.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .normal)
    self.VM.VC.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .selected)
    self.VM.VC.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .highlighted)
    self.VM.VC.timeSlider.value = 0
  }
}
//MARK: - Initial
extension SetupDetail {
  
  //MARK: - Inition
  convenience init(viewModel: DetailViewModel) {
    self.init()
    self.VM = viewModel
  }
}


