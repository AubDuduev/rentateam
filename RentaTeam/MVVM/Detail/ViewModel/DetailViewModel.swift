
import Foundation

class DetailViewModel: VMManagers {
	
	public var detailModel: DetailModel = .loading {
		didSet{
			self.commonLogicDetail()
		}
	}
  
  //MARK: - Public variable
  public var managers: DetailManagers!
  public var VC      : DetailViewController!
  
  public func viewDidLoad() {
    self.detailModel = .loading
    self.managers.setup.setupLottie()
    self.managers.setup.player()
    self.managers.setup.playButton()
    self.managers.setup.addObserverReturnTimeVideoPlayer()
    self.managers.setup.timeSlider()
    self.managers.setup.playerEnd()
  }
  func viewWillDisappear() {
    self.managers.logic.pause()
  }
  
  public func commonLogicDetail(){
    
    switch self.detailModel {
      //1 - Загрузка
      case .loading:
        self.detailModel = .errorHandler(self.VC.detailData)
      //2 - Проверяем на ошибки
      case .errorHandler(let detailData):
        guard let detailData = detailData, detailData.artist != nil else {
          AlertEK.dеfault(title: .error, message: .errorUnknown)
          return
        }
        self.detailModel = .presentData(detailData)
      //3 - Презентуем данные
      case .presentData(let detailData):
        
        self.managers.present.image(artist: detailData.artist)
        self.managers.present.data(artist: detailData.artist)
    }
  }
}
//MARK: - Initial
extension DetailViewModel {
  
  convenience init(viewController: DetailViewController) {
    self.init()
    self.VC       = viewController
    self.managers = DetailManagers(viewModel: self)
  }
}
