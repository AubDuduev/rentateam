
import UIKit

class MainCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: MainViewModel!
  
  public var artist     : DECArtist!
  public var setupLottie: GDLottieSetup!
  public var player     = GDPlayer()
  public var indexPath  : IndexPath!
  
  @IBOutlet weak var previewImageView   : UIImageView!
  @IBOutlet weak var artistNameLabel    : UILabel!
  @IBOutlet weak var trackNameLabel     : UILabel!
  @IBOutlet weak var collectionNameLabel: UILabel!
  @IBOutlet weak var lottieView         : UIView!
  @IBOutlet weak var playButton         : UIButton!
  @IBOutlet weak var timeSlider         : UISlider!
  @IBOutlet weak var timeEndLabel       : UILabel!
  @IBOutlet weak var timeBeginLabel     : UILabel!
  
  public func configure(viewModal: MainViewModel?, artist: DECArtist?, indexPath: IndexPath){
    self.viewModal = viewModal
    self.artist    = artist
    self.indexPath = indexPath
    self.viewModal.managers.present.image(cell: self)
    self.viewModal.managers.present.data(cell: self)
    self.viewModal.managers.setup.setupLottie(cell: self)
    self.viewModal.managers.setup.player(cell: self)
    self.viewModal.managers.setup.playButton(cell: self)
    self.viewModal.managers.setup.addObserverReturnTimeVideoPlayer(cell: self)
    self.viewModal.managers.setup.timeSlider(cell: self)
    self.viewModal.managers.setup.playerEnd(cell: self)
  }
  @IBAction func playButton(button: UIButton){
    self.viewModal.managers.logic.playPause(cell: self)
  }
  @IBAction func sliderButton(slider: UISlider){
    self.viewModal.managers.present.changeValueSlider(cell: self, slider: slider)
  }

  @IBAction func pushDetailVCButton(button: UIButton){
    print(#function, #line, #file)
    self.viewModal.VC.changeCurrentCell = self.indexPath
    self.viewModal.managers.logic.logicPushDetailVC(artist: self.artist)
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.cornerRadius(9, false)
    self.shadowColor(color: .black, radius: 10)
    self.previewImageView.cornerRadius(9, true)
    self.contentView.cornerRadius(9, true)
  }
}
