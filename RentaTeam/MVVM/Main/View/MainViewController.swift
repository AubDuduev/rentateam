import UIKit

class MainViewController: UIViewController {
   
   //MARK: - ViewModel
   lazy public var viewModel = MainViewModel(viewController: self)
   
   
   //MARK: - Public variable
   public var changeCurrentCell: IndexPath!
   
   //MARK: - Private variable
   private var closure:  Сlosure<String>!
   
   //MARK: - Outlets
   @IBOutlet weak var activityView      : UIView!
   @IBOutlet weak var searchBar         : UISearchBar!
   @IBOutlet weak var mainCollection    : MainCollection!
   @IBOutlet weak var mainCollectionView: UICollectionView!
   
   //MARK: - LifeCycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.viewModel.viewDidLoad()
   }
}
