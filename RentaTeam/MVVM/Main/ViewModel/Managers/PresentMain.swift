
import UIKit
import SDWebImage
import SkeletonView
import AVKit

class PresentMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func collection(artists: [DECArtist]){
    self.VM.VC.mainCollection.artists = artists
    self.VM.VC.mainCollection.collectionView.reloadData()
  }
  
  
}
//MARK: - Initial
extension PresentMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}


//MARK: - MainCollectionCell
extension PresentMain {
 
  public func image(cell: MainCollectionCell){
    guard let imageURL = cell.artist.collectionURL else { return }
    guard let url = URL(string: imageURL) else { return }
    cell.previewImageView.isSkeletonable = true
    cell.previewImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .white))
    cell.previewImageView.sd_setImage(with: url) { [weak cell] (_, error, _, url) in
      
      if let error = error {
        print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
      } else {
        cell?.previewImageView.hideSkeleton()
      }
    }
  }
  public func data(cell: MainCollectionCell){
    cell.artistNameLabel.text     = cell.artist.artistName
    cell.trackNameLabel.text      = cell.artist.trackName
    cell.collectionNameLabel.text = cell.artist.collectionName
  }
  public func changeValueSlider(cell: MainCollectionCell, slider: UISlider){
    let slider  = slider.value
    let seconds = cell.player.player.duration()?.seconds ?? 0
    let value   = CMTimeValue(slider * seconds)
    let seeTime = CMTime(value: value, timescale: 1)
    cell.player.player.seek(to: seeTime)
  }
  public func videoTimeEnd(cell: MainCollectionCell){
    guard cell.player.control.isPlay else { return }
    let second  = cell.player.player.duration()?.second ?? 0
    let minutes = cell.player.player.duration()?.minutes ?? 0
    cell.timeEndLabel.text = String(format: .format(.two), minutes, second)
  }
  public func videoTimeStart(cell: MainCollectionCell, currentTime: CMTime){
    guard cell.player.control.isPlay else { return }
    let seconds = CMTimeGetSeconds(currentTime)
    let second  = Int(seconds) % 60
    let minutes = Int(seconds) / 60
    cell.timeBeginLabel.text = String(format: .format(.two), minutes, second)
  }
  public func videoTimeSlider(cell: MainCollectionCell, currentTime: CMTime){
    let currentSeconds = Float(CMTimeGetSeconds(currentTime))
    let totalSeconds   = cell.player.player.duration()?.seconds ?? 0
    let sliderValue    = Float(currentSeconds / totalSeconds)
    cell.timeSlider.value = sliderValue
    guard sliderValue == 1.0 else { return }
    cell.timeSlider.value = 0
    cell.player.control.pause()
  }
}

