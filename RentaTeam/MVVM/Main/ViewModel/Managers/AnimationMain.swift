import UIKit

class AnimationMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func animationCell(cell: MainCollectionCell){
    UIView.animate(withDuration: 4) { [weak self] in
      guard let self = self else { return }
      cell.frame.size.width  = self.VM.VC.view.frame.width
      cell.frame.size.height = self.VM.VC.view.frame.height
      self.VM.VC.view.layoutIfNeeded()
    }
  }
}
//MARK: - Initial
extension AnimationMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}

