import Foundation
import UIKit

class LogicMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func loading(){
    self.VM.mainModel = .loading
  }
  public func getData(nameArtist: String){
    self.VM.mainModel = .getData(nameArtist)
  }
  public func playPause(cell: MainCollectionCell){
    if cell.player.control.isPlay {
      cell.player.control.pause()
      cell.setupLottie.stop()
      cell.playButton.setImage(UIImage.set(system: .pause), for: .normal)
    } else {
      self.getAllMainCollectionCell()
      cell.player.control.play()
      cell.setupLottie.play()
      cell.playButton.setImage(UIImage.set(system: .play), for: .normal)
    }
  }
  public func delayAfter(){
    //1 - Отмена запрос на поиск
    DelayAfter.shared.cancelDelete()
    DelayAfter.shared.deleteAfter(seconds: 1.5)
    //2 - Анимация поиска
    self.VM.managers.logic.activityLoadingView(show: true)
    //3 - Запрос на сервер
    DelayAfter.shared.execute = { [weak self] in
      guard let self = self else { return }
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        self.searchArtist(textSearch: self.VM.VC.searchBar.text)
        self.resignFirstResponder()
      }
    }
  }
  public func logicPushDetailVC(artist: DECArtist){
    print(#function, #line, #file)
    let detailData = DetailData(artist: artist)
    self.VM.managers.router.push(.DetailVC(detailData))
  }
  public func searchArtist(textSearch: String?){
    if let textSearch = textSearch, !textSearch.isEmpty {
      self.getData(nameArtist: textSearch)
    } else {
      self.VM.managers.logic.activityLoadingView(show: false)
    }
  }
  public func getAllMainCollectionCell(){
    let cells = self.VM.VC.mainCollectionView.visibleCells as? [MainCollectionCell]
    cells?.forEach({ $0.player.control.pause() })
    cells?.forEach({ $0.setupLottie.stop() })
    cells?.forEach({ $0.playButton.setImage(UIImage.set(system: .pause), for: .normal)})
  }
  public func pausePlayerWillEnd(cell: UICollectionViewCell){
    if let mainCel = cell as? MainCollectionCell {
      mainCel.player.control.pause()
      mainCel.setupLottie?.stop()
    }
  }
  public func resignFirstResponder(){
    self.VM.VC.searchBar.resignFirstResponder()
  }
  public func activityLoadingView(show: Bool){
    if show {
      self.VM.VC.activityView.isHidden = false
      self.VM.loading.startAnimating()
    } else {
      self.VM.VC.activityView.isHidden = true
      self.VM.loading.stopAnimating()
    }
  }
}
//MARK: - Initial
extension LogicMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}
