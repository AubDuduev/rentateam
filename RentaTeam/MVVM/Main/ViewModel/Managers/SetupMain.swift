
import UIKit
import NVActivityIndicatorView
import SnapKit

class SetupMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func mainCollection(){
    let collectionViewLayout = MainCollectionViewLayout()
    collectionViewLayout.sectionInset = .init(top: 30, left: 0, bottom: 30, right: 0)
    collectionViewLayout.sectionInsetReference = .fromContentInset
    collectionViewLayout.minimumLineSpacing = 20
    self.VM.VC.mainCollectionView.collectionViewLayout = collectionViewLayout
    self.VM.VC.mainCollection.viewModal                = self.VM
  }
  public func searchBar(){
    self.VM.VC.searchBar.delegate = self.VM.VC
  }
  public func setupActivityView(){
    let rect = CGRect(x: 0, y: 0, width: 200, height: 200)
    self.VM.loading = NVActivityIndicatorView(frame  : rect,
                                              type   : .lineScale,
                                              color  : .black,
                                              padding: 10)
    self.VM.VC.activityView.addSubview(self.VM.loading)
    self.VM.loading.snp.makeConstraints { [weak self] (loading) in
      guard let self = self else { return }
      loading.edges.equalTo(self.VM.VC.activityView)
    }
  }
}
//MARK: - Initial
extension SetupMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}


//MARK: - MainCollectionCell
extension SetupMain {
  
  public func setupLottie(cell: MainCollectionCell){
    cell.setupLottie = GDLottieSetup(view: cell.lottieView)
    let name = GDLottieSetup.LottieName.equalizer.rawValue
    cell.setupLottie.setup(size: 0, padding: 0)
    cell.setupLottie.added(name: name, loopMode: .autoReverse)
  }
  public func player(cell: MainCollectionCell){
    cell.player.load(urlPlayer: .network(cell.artist?.trackURL), typeURL: .mp4)
  }
  public func playButton(cell: MainCollectionCell){
    cell.playButton.setImage(UIImage.set(system: .pause), for: .normal)
  }
  public func addObserverReturnTimeVideoPlayer(cell: MainCollectionCell){
    cell.player.control.observerReturnTime = { [weak self] (time) in
      guard let self = self else { return }
      //3 - set value slider
      self.VM.managers.present.videoTimeSlider(cell: cell, currentTime: time)
      //4 - set time begin label
      self.VM.managers.present.videoTimeStart(cell: cell, currentTime: time)
      //5 - set time end label
      self.VM.managers.present.videoTimeEnd(cell: cell)
    }
  }
  public func playerEnd(cell: MainCollectionCell){
    cell.player.control.observerEnd = { [weak cell] in
      guard let cell = cell else { return }
      cell.player.control.pause()
      cell.setupLottie.stop()
    }
  }
  public func timeSlider(cell: MainCollectionCell){
    cell.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .normal)
    cell.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .selected)
    cell.timeSlider.setThumbImage(#imageLiteral(resourceName: "videoSliderTime"), for: .highlighted)
    cell.timeSlider.value = 0
  }
}
