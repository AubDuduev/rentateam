import UIKit

class ServerMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  public func getArtists(artistName: String, completion: @escaping Сlosure<DECSearchResult>){
    //Request
    self.VM.server.request(requestType: GETArtists(), data: artistName) { [weak self] (serverResult) in
      guard let self = self else { return }
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerMain ->, function: getArtists -> data: DECSearchResult ->, description: ", error.localizedDescription)
          AlertEK.dеfault(title: .error, message: .noJSON){ [weak self] in
            guard let self = self else { return }
            self.getArtists(artistName: artistName, completion: completion)
          }
        //Susses
        case .object(let object):
          let searchResult = object as! DECSearchResult
          completion(searchResult)
          print("Successful data: class: ServerMain ->, function: getArtists ->, data: DECSearchResult")
          
      }
    }
  }
}
//MARK: - Initial
extension ServerMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}


