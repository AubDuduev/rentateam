
import UIKit

class RouterMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModel!
  
  
  public func push(_ type: Push){
    
    switch type {
      case .DetailVC(let detailData):
        self.pushDetailVC(detailData: detailData)
    }
  }
  
  enum Push {
    case DetailVC(DetailData)
  }
}
//MARK: - Private functions
extension RouterMain {
  
  private func pushDetailVC(detailData: DetailData){
    let detailVC = self.VM.VC.getVCForID(storyboardID     : .Detail,
                                         vcID             : .DetailVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! DetailViewController
    detailVC.detailData = detailData
    print(#function, #line, #file)
    detailVC.transitioningDelegate = self.VM.VC
    self.VM.VC.present(detailVC, animated: true)
  }
}
//MARK: - Initial
extension RouterMain {
  
  //MARK: - Inition
  convenience init(viewModel: MainViewModel) {
    self.init()
    self.VM = viewModel
  }
}



