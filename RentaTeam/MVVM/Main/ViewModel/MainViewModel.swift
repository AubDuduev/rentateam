
import Foundation
import NVActivityIndicatorView

class MainViewModel: VMManagers {
   
   public var mainModel: MainModel = .loading {
      didSet{
         self.mainLogic()
      }
   }
   
   //MARK: - Public variable
   public let errorHandler = ErrorHandlerSearchResult()
   public var managers     : MainManagers!
   public var VC           : MainViewController!
   public var server       = Server()
   public var loading      : NVActivityIndicatorView!
   
   public func viewDidLoad() {
      self.managers.setup.mainCollection()
      self.managers.setup.setupActivityView()
      self.managers.setup.searchBar()
      self.managers.logic.loading()
   }
   
   public func mainLogic(){
      
      switch self.mainModel {
         //1 - Загрузка
         case .loading:
            self.managers.logic.activityLoadingView(show: true)
            self.managers.logic.getData(nameArtist: "Madonna")
            
         //2 - Получаем данные
         case .getData(let artistName):
            self.managers.server.getArtists(artistName: artistName) { (searchResult) in
               self.mainModel = .errorHandler(searchResult)
            }
         //3 - Проверяем на ошибки
         case .errorHandler(let searchResult):
            guard self.errorHandler.check(searchResult: searchResult) else {
               self.managers.logic.activityLoadingView(show: true)
               return
            }
            let mainData = MainData(artists: searchResult?.results)
            self.mainModel = .presentData(mainData)
            
         //4 - Презентуем данные
         case .presentData(let mainData):
            self.managers.present.collection(artists: mainData.artists)
            self.managers.logic.activityLoadingView(show: false)
      }
   }
}
//MARK: - Initial
extension MainViewModel {
   
   convenience init(viewController: MainViewController) {
      self.init()
      self.VC       = viewController
      self.managers = MainManagers(viewModel: self)
   }
}
