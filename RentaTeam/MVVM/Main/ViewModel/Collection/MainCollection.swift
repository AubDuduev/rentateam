
import UIKit

class MainCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : MainViewModel!
  public var artists       : [DECArtist]!
}

//MARK: - Delegate CollectionView
extension MainCollection: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
  
  func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    self.viewModal.managers.logic.pausePlayerWillEnd(cell: cell)
  }
}

//MARK: - DataSource CollectionView
extension MainCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
    return self.artists?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = MainCollectionCell().collectionCell(collectionView, indexPath: indexPath)
    cell.configure(viewModal: viewModal, artist: self.artists?[indexPath.row], indexPath: indexPath)
    return cell
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension MainCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = collectionView.frame.width - 50
    let height: CGFloat = 200
    return .init(width: width, height: height)
  }
  func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
    self.viewModal.managers.logic.resignFirstResponder()
  }
}


