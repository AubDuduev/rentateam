
import UIKit

class MainDismissed: NSObject, UIViewControllerAnimatedTransitioning {
  
  private let duration = 3.0
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    self.animation(transitionContext: transitionContext)
  }
}

//Animation
extension MainDismissed {
  
  private func animation(transitionContext: UIViewControllerContextTransitioning){
    guard let _ = transitionContext.viewController(forKey: .from) else { return }
    guard let _ = transitionContext.viewController(forKey: .to)   else { return }
   // transitionContext.containerView.addSubview(fromVC.view)
    //transitionContext.containerView.addSubview(toVC.view)
    
    //container.addSubview(toVC.view)
    
    transitionContext.completeTransition(true)
  }
}
