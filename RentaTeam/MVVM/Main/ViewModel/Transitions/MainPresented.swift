
import UIKit

class MainPresented: NSObject, UIViewControllerAnimatedTransitioning {
  
  private let duration = 3.0
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return duration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    self.animation(transitionContext: transitionContext)
  }
}

//Animation
extension MainPresented {
  
  private func animation(transitionContext: UIViewControllerContextTransitioning){
    guard let fromVC = transitionContext.viewController(forKey: .from) as? MainViewController   else { return }
    guard let toVC   = transitionContext.viewController(forKey: .to)   as? DetailViewController else { return }
    transitionContext.containerView.addSubview(toVC.view)
    transitionContext.containerView.addSubview(fromVC.view)
    let newFromVC = transitionContext.viewController(forKey: .from)  as! MainViewController
    let cellOne = newFromVC.mainCollectionView.cellForItem(at: newFromVC.changeCurrentCell) as! MainCollectionCell
    let frame = CGRect(x: newFromVC.view.frame.origin.x - 1500,
                       y: newFromVC.view.frame.origin.y - 1500,
                       width: newFromVC.view.frame.width + 3000,
                       height: newFromVC.view.frame.height + 3000)
    
    UIView.animateKeyframes(withDuration: 1, delay: 0, options: [.calculationModeCubicPaced]) {
      UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.1) {
        newFromVC.searchBar.alpha = 0
        newFromVC.mainCollectionView.bringSubviewToFront(cellOne)
        cellOne.artistNameLabel.alpha     = 0
        cellOne.trackNameLabel.alpha      = 0
        cellOne.collectionNameLabel.alpha = 0
        cellOne.cornerRadius(0, true)
        cellOne.frame = frame
        newFromVC.view.layoutIfNeeded()
      }
      UIView.addKeyframe(withRelativeStartTime: 0.9, relativeDuration: 5) {
        newFromVC.view.alpha = 0
      }
     
    } completion: { (finish) in
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        transitionContext.completeTransition(true)
        newFromVC.view.alpha = 1
        newFromVC.searchBar.alpha = 1
        cellOne.artistNameLabel.alpha = 1
        cellOne.trackNameLabel.alpha  = 1
        cellOne.collectionNameLabel.alpha = 1
        fromVC.mainCollectionView.reloadData()
      }
    }
    
  }
}
