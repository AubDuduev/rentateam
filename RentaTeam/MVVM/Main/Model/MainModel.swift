
import UIKit

enum MainModel {
	
	case loading
	case getData(String)
	case errorHandler(DECSearchResult?)
	case presentData(MainData)
	
	
}

