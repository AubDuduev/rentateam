//
//  AppDelegate.swift
//  RentaTeam
//
//  Created by Senior Developer on 13.02.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  public var window: UIWindow?

  private let rootVC = RootVC()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    self.rootVC.set(window: self.window)
    return true
  }

}

